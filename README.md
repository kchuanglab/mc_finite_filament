# Monte Carlo code for twisting finite filament

This is a repository for recreating the plots for the Monte Carlo simulation results.

## Code environment
The scripts have been tested using MATLAB R2018a.

Note that the scripts need to call a mex function (`filament_update_finite.mexmaci64`). Since mex functions are system-dependent, the included file only works with Mac system (tested on MacOS 10.14.6). For other systems, the mex file needs to be re-built using appropriate Cpp compilers (see <https://www.mathworks.com/help/matlab/call-mex-file-functions.html> for details). The included `mexmaci64` file was compiled using Xcode with Clang (Xcode 11.3).

## How to run this package
The raw data for plots are included in the directory `MC_finite_data`. To directly generate the plot (Figure 5c), run `plot_fig4f_finite.m` in MATLAB.

To generate the raw data for a different finite filament, run `MC_finite_limitlength_varyingw.m`. Note that this function calls `filament_update_finite.mexmaci64`.

## Questions
For questions related to this repository, email to Handuo Shi (handuo@stanford.edu).