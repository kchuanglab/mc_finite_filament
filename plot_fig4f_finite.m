clear all;
close all;

kind = [1 2 5];



%%
colors = lines(7);

shades = [0 0.15 0.3 0.45];

figure('color', 'w');
hold on;

for i = 1 : length(kind)
    k = kind(i);
    if k == 1
        data = load('MC_finite_data/short_filament_limit_length.mat');
    else
        data = load(strcat('MC_finite_data/short_filament_limit_length_k0_', num2str(k), '.mat'));
    end
    thetadata = data.finaltheta;
    mtheta = mean(thetadata);
    stheta = std(thetadata);
    mtheta(stheta > 0.05) = 90 / 180 * pi;
    mtheta = mtheta ./ pi .* 180;
    mtheta = 180 - mtheta;
    disp(mtheta);
    w0 = abs(data.w);
    w1 = linspace(0.01, 0.06, 200);
    l1 = interp1(w0, mean(abs(data.l0)), w1, 'linear');
    theta1 = interp1(w0, mtheta, w1, 'linear');

    if i == 1
        ax = plotyy(w1, l1, w1, theta1);
        hold(ax(1), 'on');
        hold(ax(2), 'on');
    else
        plot(ax(1), w1, l1, 'color', tint_color(colors(1, :), shades(i)));
        plot(ax(2), w1, theta1, 'color', tint_color(colors(2, :), shades(i)));
    end
    plot(ax(1), w0, abs(mean(data.l0)), ...
        '.', 'MarkerSize', 15, 'LineStyle', 'none', 'color', tint_color(colors(1, :), shades(i)));
    plot(ax(2), w0, mtheta, ...
        '.', 'MarkerSize', 15, 'LineStyle', 'none', 'color', tint_color(colors(2, :), shades(i)));
    
end

hold off;
ylim(ax(2), [90, 140]);
set(ax(2), 'YTick', 90 : 10 : 140);
xlim(ax(1), [0.015 0.06]);
xlim(ax(2), [0.015 0.06]);
ylim(ax(1), [0 2000]);
xlabel('Intrinsic filament twisting (nm^{-1})');
ylabel(ax(1), 'Filament limit length (nm)');
ylabel(ax(2), 'MreB pitch angle (deg)');
% xlim([0.01 0.05]);



% save('fit_length_orientation.mat', 'p1', 'p2', 'w');