#include <cmath>
#include <iostream>
#include "mex.h"

using namespace std;

/*
 * MC for filament
 *
 * update using MC method
 *
 * This is a MEX-file for MATLAB.
 *
 */
 
int updateTheta(double *theta, double *psi, double C, double K, double V, double r, double w0, double k0, double dL, int sections, double thetaStep);
int updatePsi(double *theta, double *psi, double C, double K, double V, double r, double w0, double k0, double dL, int sections, double psiStep);
double energy(int index, double *theta, double *psi, double C, double K, double V, double r, double w0, double k0, double dL, int sections);
double filamentEnergy(double *theta, double *psi, double C, double K, double V, double r, double w0, double k0, double dL, int sections);

int update(double *outTheta, double *outPsi, double *theta, double *psi, double C, double K, double V, double r, double w0, double k0, double dL, int sections, double thetaStep, double psiStep, int totalStep, double *iniE, double *endE, int curStep)
{
    /** random number generator **/
    if (curStep == 0)
        srand(time(NULL));
    // run the update for MC sampling
    int sscount = 20;
    double prevE = filamentEnergy(theta, psi, C, K, V, r, w0, k0, dL, sections);
    double curE;
    iniE[0] = prevE;
    for (int i = 0; i < totalStep; i++)
    {
        updateTheta(theta, psi, C, K, V, r, w0, k0, dL, sections, thetaStep);
        updatePsi(theta, psi, C, K, V, r, w0, k0, dL, sections, psiStep);
        if (i % 1000 == 0)
        {
            curE = filamentEnergy(theta, psi, C, K, V, r, w0, k0, dL, sections);
            cout << "Current energy: "<< curE << endl;
            /*
            if ((abs(curE - prevE) / curE) < 0.01)
            {
                sscount--;
            }
            else
            {
                sscount = 20;
            }
             */
            prevE = curE;
        }
        if (sscount == 0)
            break;
    }
    /*
    if (sscount > 0)
    {
        cout << "Equilibrium not reached!" << endl;
        //return 1;
    }
     */

    for (int i = 0; i < sections; i ++)
    {
        outTheta[i] = theta[i];
        outPsi[i] = psi[i];
    }
    endE[0] = curE;
    return 0;
}



void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] )
{
  double *x,*y;
  size_t mrows,ncols;
  
  /* Check for proper number of arguments. */
  if(nrhs != 14) {
    mexErrMsgIdAndTxt( "MATLAB:timestwo:invalidNumInputs",
            "14 inputs required.");
  } else if(nlhs != 4) {
    mexErrMsgIdAndTxt( "MATLAB:timestwo:maxlhs",
            "4 outputs required");
  }
  
  /* Check input formatting */
  // input 1: theta -- double matrix
  if( !mxIsDouble(prhs[0]) || mxIsComplex(prhs[0])) 
  {
    mexErrMsgIdAndTxt("MyToolbox:arrayProduct:notDouble",
        "Input matrix must be type double.");
  }
  
  // input 2: psi -- double matrix
  if( !mxIsDouble(prhs[1]) || mxIsComplex(prhs[1])) 
  {
    mexErrMsgIdAndTxt("MyToolbox:arrayProduct:notDouble",
        "Input matrix must be type double.");
  }
  
  // input 3 to 9: double scalers
  for (int i = 2; i < 9; i++)
  {
      if( !mxIsDouble(prhs[i]) || mxIsComplex(prhs[i]) ||
            mxGetNumberOfElements(prhs[i]) != 1 ) 
      {
        mexErrMsgIdAndTxt("MyToolbox:arrayProduct:notScalar",
                      "Input must be a scalar.");
      }
  }
  
  // input 10: sections, int
  if( !mxIsDouble(prhs[9]) || mxGetNumberOfElements(prhs[9]) != 1 ) 
  {
    mexErrMsgIdAndTxt("MyToolbox:arrayProduct:notScalar",
                  "Input must be a scalar.");
  }
  
  // input 11 to 12: double scalers
  for (int i = 10; i < 12; i++)
  {
      if( !mxIsDouble(prhs[i]) || mxIsComplex(prhs[i]) ||
            mxGetNumberOfElements(prhs[i]) != 1 ) 
      {
        mexErrMsgIdAndTxt("MyToolbox:arrayProduct:notScalar",
                      "Input must be a scalar.");
      }
  }
  
  // input 13: totalStep, int
  if( !mxIsDouble(prhs[12]) || mxGetNumberOfElements(prhs[12]) != 1 ) 
  {
    mexErrMsgIdAndTxt("MyToolbox:arrayProduct:notScalar",
                  "Input must be a scalar.");
  }
  
  // input 14: curStep, int
  if( !mxIsDouble(prhs[13]) || mxGetNumberOfElements(prhs[13]) != 1 ) 
  {
    mexErrMsgIdAndTxt("MyToolbox:arrayProduct:notScalar",
                  "Input must be a scalar.");
  }

  
  /* Create matrix for the return arguments. */
  mrows = mxGetM(prhs[0]);
  ncols = mxGetN(prhs[0]);
  plhs[0] = mxCreateDoubleMatrix((mwSize)mrows, (mwSize)ncols, mxREAL);
  
  mrows = mxGetM(prhs[1]);
  ncols = mxGetN(prhs[1]);
  plhs[1] = mxCreateDoubleMatrix((mwSize)mrows, (mwSize)ncols, mxREAL);
  
  /* Declare variables for input/output argument and get data */
  double *initheta = mxGetPr(prhs[0]);
  double *inipsi = mxGetPr(prhs[1]);
  double C = mxGetScalar(prhs[2]);
  double K = mxGetScalar(prhs[3]);
  double V = mxGetScalar(prhs[4]);
  double r = mxGetScalar(prhs[5]);
  double w0 = mxGetScalar(prhs[6]);
  double k0 = mxGetScalar(prhs[7]);
  double dL = mxGetScalar(prhs[8]);
  int sections = mxGetScalar(prhs[9]);
  double thetaStep = mxGetScalar(prhs[10]);
  double psiStep = mxGetScalar(prhs[11]);
  int totalStep = mxGetScalar(prhs[12]);
  int curStep = mxGetScalar(prhs[13]);
  
  double *outTheta;
  double *outPsi;
  double *iniE;
  double *endE;
  
  plhs[0] = mxCreateDoubleMatrix(1, ncols, mxREAL);
  plhs[1] = mxCreateDoubleMatrix(1, ncols, mxREAL);
  plhs[2] = mxCreateDoubleMatrix(1, 1, mxREAL);
  plhs[3] = mxCreateDoubleMatrix(1, 1, mxREAL);
  
  outTheta = mxGetPr(plhs[0]);
  outPsi = mxGetPr(plhs[1]);
  iniE = mxGetPr(plhs[2]);
  endE = mxGetPr(plhs[3]);
  
  mrows = mxGetM(plhs[0]);
  ncols = mxGetN(plhs[0]);
  double *theta = new double[ncols];
  double *psi = new double[ncols];
  
  for (int i = 0; i < ncols; i ++)
  {
      theta[i] = initheta[i];
      psi[i] = inipsi[i];
  }
  
  /* Call the subroutine. */
  update(outTheta, outPsi, theta, psi, C, K, V, r, w0, k0, dL, sections, thetaStep, psiStep, totalStep, iniE, endE, curStep);
  
  delete[] theta;
  delete[] psi;
}



int updateTheta(double *theta, double *psi, double C, double K, double V, double r, double w0, double k0, double dL, int sections, double thetaStep)
{
    double e0, eNew;
    double tempTheta[sections];
    
    // looping through each segment
    for (int i = 0; i <= sections; i++)
    {
        if (i == sections)
        {
            double mcdtheta = (double(rand()) / double(RAND_MAX) - 0.5) * thetaStep * 10;
            for (int j = 0; j < sections; j++)
            {
                tempTheta[j] = theta[j] + mcdtheta;
            }
            e0 = filamentEnergy(theta, psi, C, K, V, r, w0, k0, dL, sections);
            eNew = filamentEnergy(tempTheta, psi, C, K, V, r, w0, k0, dL, sections);
        }
        else
        {
            double mcdtheta = (double(rand()) / double(RAND_MAX) - 0.5) * thetaStep;
            for (int j = 0; j < sections; j++)
            {
                tempTheta[j] = theta[j];
            }

            tempTheta[i] += mcdtheta;

            // summing up three energies: i - 1, i, and i + 1
            e0 = 0.;
            eNew = 0.;

            // energy for i - 1: only for i > 1
            if (i > 0)
            {
                e0 += energy(i - 1, theta, psi, C, K, V, r, w0, k0, dL, sections);
                eNew += energy(i - 1, tempTheta, psi, C, K, V, r, w0, k0, dL, sections);
            }

            // energy for i -- every segment
            e0 += energy(i, theta, psi, C, K, V, r, w0, k0, dL, sections);
            eNew += energy(i, tempTheta, psi, C, K, V, r, w0, k0, dL, sections);

            // energy for i + 1: only for i < sections - 1
            if (i < sections - 1)
            {
                e0 += energy(i + 1, theta, psi, C, K, V, r, w0, k0, dL, sections);
                eNew += energy(i + 1, tempTheta, psi, C, K, V, r, w0, k0, dL, sections);
            }
        }
        
        double deltaE = eNew - e0;
        if (deltaE <= 0)
        {
            //accept new configuration
            if (i < sections)
            {
                theta[i] = tempTheta[i];
            }
            else
            {
                for (int j = 0; j < sections; j++)
                {
                    theta[j] = tempTheta[j];
                }
            }
        }
        else
        {
            // conditioning for e^{-deltaE}
            double pAcc = double(rand()) / double(RAND_MAX);
            if (pAcc < exp(-deltaE * 100.))
            {
                //accept at a probability of exp(-deltaE)
                if (i < sections)
                {
                    theta[i] = tempTheta[i];
                }
                else
                {
                    for (int j = 0; j < sections; j++)
                    {
                        theta[j] = tempTheta[j];
                    }
                }
            }
        }
    }
    
    return 0;
}


int updatePsi(double *theta, double *psi, double C, double K, double V, double r, double w0, double k0, double dL, int sections, double psiStep)
{
    double e0, eNew;
    double tempPsi[sections];
    
    // looping through each segment
    for (int i = sections; i >= 0; i--)
    //for (int i = 1; i <= sections; i++)
    {
        if (i == sections)
        {
            double mcdpsi = (double(rand()) / double(RAND_MAX) - 0.5) * psiStep * 10;
            for (int j = 0; j < sections; j++)
            {
                tempPsi[j] = psi[j] + mcdpsi;
            }
            e0 = filamentEnergy(theta, psi, C, K, V, r, w0, k0, dL, sections);
            eNew = filamentEnergy(theta, tempPsi, C, K, V, r, w0, k0, dL, sections);
        }
        else
        {
            double mcdpsi = (double(rand()) / double(RAND_MAX) - 0.5) * psiStep;
            
            for (int j = 0; j < sections; j++)
            {
                tempPsi[j] = psi[j];
            }

            tempPsi[i] += mcdpsi;

            // summing up three energies: i - 1, i, and i + 1
            e0 = 0.;
            eNew = 0.;

            // energy for i - 1: only for i > 1
            if (i > 0)
            {
                e0 += energy(i - 1, theta, psi, C, K, V, r, w0, k0, dL, sections);
                eNew += energy(i - 1, theta, tempPsi, C, K, V, r, w0, k0, dL, sections);
            }

            // energy for i -- every segment
            e0 += energy(i, theta, psi, C, K, V, r, w0, k0, dL, sections);
            eNew += energy(i, theta, tempPsi, C, K, V, r, w0, k0, dL, sections);

            // energy for i + 1: only for i < L - 1
            if (i < sections - 1)
            {
                e0 += energy(i + 1, theta, psi, C, K, V, r, w0, k0, dL, sections);
                eNew += energy(i + 1, theta, tempPsi, C, K, V, r, w0, k0, dL, sections);
            }
        }
        
        double deltaE = eNew - e0;
        if (deltaE <= 0)
        {
            //accept new configuration
            psi[i] = tempPsi[i];
        }
        else
        {
            // conditioning for e^{-deltaE}
            double pAcc = double(rand()) / double(RAND_MAX);
            if (pAcc < exp(-deltaE * 100.))
            {
                //accept at a probability of exp(-deltaE)
                psi[i] = tempPsi[i];
            }
        }
    }
    
    return 0;
}

double energy(int index, double *theta, double *psi, double C, double K, double V, double r, double w0, double k0, double dL, int sections)
{
    // calculating energy contribution of segment #index
    
    double curTheta, curPsi, curDTheta, curDPsi;
    curTheta = theta[index];
    curPsi = psi[index];
    if (index == 0)
    {
        // dtheta and dpsi for the first section
        curDTheta = (theta[1] - theta[0]) / dL;
        curDPsi = (psi[1] - psi[0]) / dL;
    }
    else if (index == sections - 1)
    {
        // dtheta and dpsi for the last section
        curDTheta = (theta[sections - 1] - theta[sections - 2]) / dL;
        curDPsi = (psi[sections - 1] - psi[sections - 2]) / dL;
    }
    else
    {
        // dtheta and dpsi for middle sections
        curDTheta = (theta[index + 1] - theta[index - 1]) / 2. / dL;
        curDPsi = (psi[index + 1] - psi[index - 1]) / 2. / dL;
    }
    
    //double ebending = C * pow(sqrt(pow(curDTheta, 2.) + pow(sin(curTheta), 4.) / pow(r, 2.)) - k0, 2.);
    double ebending = dL * C * pow(pow(sin(curTheta), 2.) / r - k0, 2.) + dL * C * pow(curDTheta, 2.);
    double etwisting = dL * K * pow((curDPsi - sin(2. * curTheta) / 2. / r - w0), 2.);
    double ebinding = dL * V * pow(sin(curPsi / 2.), 2.);
    return 0.5 * (ebending + etwisting) + ebinding;
}


double filamentEnergy(double *theta, double *psi, double C, double K, double V, double r, double w0, double k0, double dL, int sections)
{
    double filamentEnergy = 0;
    for (int i = 0; i < sections; i++)
    {
        filamentEnergy += energy(i, theta, psi, C, K, V, r, w0, k0, dL, sections);
    }
    return filamentEnergy;
}
