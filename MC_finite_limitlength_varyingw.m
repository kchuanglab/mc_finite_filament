clear all;
close all;

K = 4.6e3; % twisting elastic constant. in unit of [KbT][nm]. all other energy-related parameters are in unit of K
C = 1.4e4; % elastic constant, in unit of K
r = 400; % in nm, radius of a cell
% w0 = -0.01; % rad/nm, intrinsic twisting of ATP-bound filament
k0 = 2.3e-3; % rad/nm, intrinsic bending of filament

V = 4;
mu0 = 2;

% steps for theta and psi
thetaStep = pi / 100;
psiStep = pi / 100;

w = -0.005 : -0.005 : -0.06;
% w = [-0.01 -0.03 -0.05 -0.06];
numreps = 10;

l0 = zeros(numreps, length(w));


for iw = 1 : length(w)
    
    for rep = 1 : numreps
        
        w0 = w(iw);
    
%         L0 = 50 : 100 : 600;
        L0 = linspace(10, 1 / abs(w0) * 10, 5);
        E0 = zeros(1, length(L0));

        for iL = 1 : length(L0)
            L = L0(iL);
            sections = 200;

            dL = L / sections;

            theta = zeros(1, sections);
            psi = zeros(1, sections);

            % initial status: the filament is free, so theta = pi/2, dpsi = w0
            for i = 1 : sections
                theta(i) = pi / 2;
                psi(i) = 0;%pi / sections * (i - sections / 2) / 15;
            end

            totalStepPerLoop = 1000;
            totalLoopCount = 100;

            outTheta = cell(1, totalLoopCount + 1);
            outPsi = cell(1, totalLoopCount + 1);
            endE = zeros(1, totalLoopCount + 1);

            outTheta{1} = theta;
            outPsi{1} = psi;

            for i = 1 : totalLoopCount
                [outTheta{i + 1}, outPsi{i + 1}, curE, endE(i + 1)] = filament_update_finite(outTheta{i}, outPsi{i}, C, K, V, r, w0, k0, dL, sections, thetaStep, psiStep, totalStepPerLoop, i - 1);
                if i == 1
                    endE(1) = curE;
                end

                if i >= 10
                    lastE = endE(i - 9 : i);
                    p = polyfit(1 : 10, lastE, 1);
                    if abs(p(1)) < 0.02
                        endE = endE(1 : i);
                        outTheta = outTheta(1 : i);
                        outPsi = outPsi(1 : i);
                        break;
                    end
                end
            end


            endPsi = outPsi{end};
            endTheta = outTheta{end};
            E0(iL) = endE(end);
        end
        f = fit(L0', E0', 'a * (exp(b * x) - 1)', 'StartPoint', [100, 0.01]);
        l0(rep, iw) = log(mu0 / f.a / f.b) / f.b;
    end
end

save('./MC_finite_data/limit_length.mat', 'w', 'K', 'C', 'r', 'k0', 'V', 'mu0', 'l0');
